<?php
	session_start();
	if(isset($_SESSION['korisnickoIme'])){
		header("location: allNews.php");
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
		<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>Login</title>

		<!-- CSS -->
		<link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="css/main.css">

	</head>
	<body>
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <a class="navbar-brand" href="#">Admin panel</a>
		    </div>
		    <ul class="nav navbar-nav">
		      <li class="active"><a href="allNews.php">Login</a></li>
		      <li><a href="register.php">Registracija</a></li>
		    </ul>
		  </div>
		</nav>

		<div class="container">
			<form action="brains/authentication.php" method="POST">
		      <div class="form-group">
		        <label for="korisnickoIme">Korisnicko ime:</label>
		        <input type="text" class="form-control" id="korisnickoIme" placeholder="Unesite korisnicko ime" name="korisnickoIme">
		      </div>
		      <div class="form-group">
		        <label for="sifra">Sifra:</label>
		        <input type="password" class="form-control" id="sifra" placeholder="Unesite sifru" name="sifra">
		      </div>
		      <button type="submit" class="btn btn-default">Potvrdite</button>
		    </form>
		</div>

		<!-- Javascript -->
		<script src="js/jquery-3.2.1.js"></script>
		<script src="bootstrap/js/bootstrap.js"></script>
		<script src="js/main.js"></script>
	</body>
</html>
