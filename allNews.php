<?php
	session_start();
	if(!isset($_SESSION['korisnickoIme'])){
		header("location: login.php");
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
		<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>Admin Panel</title>

		<!-- CSS -->
		<link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="css/main.css">

	</head>
	<body>
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <a class="navbar-brand" href="#">Game of Codes</a>
		    </div>
		    <ul class="nav navbar-nav">
		      <li class="active"><a href="allNews.php">Novosti</a></li>
		      <li><a href="addNewsForm.php">Dodaj novost</a></li>
		    </ul>
		  </div>
		</nav>
		<div class="container">
			<h2>Novosti</h2>
			<p>Tabela novosti sa opcijama izmjene i brisanja</p>            
			<table class="table">
				<thead>
					<tr>
						<th>Naslov</th>
						<th>Sadrzaj</th>
						<th>Slika</th>
						<th></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php
						$con = mysqli_connect("localhost","root","","game_of_codes");
						if (mysqli_connect_errno()){
						  echo "Failed to connect to MySQL: " . mysqli_connect_error();
						}

						$sql = "SELECT * FROM novosti";
						$novosti = $con->query($sql);
						if ($novosti->num_rows > 0) { 
						    while($novost = $novosti->fetch_assoc()) {
						    	$id = $novost["id"];
						    	echo "<tr>";
						    	echo "<td>".$novost["naslov"]."</td>";
						    	echo "<td>".$novost["sadrzaj"]."</td>";
						    	echo "<td>".$novost["slika"]."</td>";
						    	echo "<td>".
						    			"<form action='editNewsForm.php' method='POST'>".
								    		"<input type='text' name='id' style='display:none;' value='$id'>".
								    		"<button type='submit' class='btn btn-default'>Izmjena</button>".
								    	"</form>".
								    "</td>";
						    	echo "<td>".
						    			"<form action='admin/deleteNews.php' method='POST'>".
						    				"<input type='text' name='id' style='display:none;' value='$id'>".
						    				"<button type='submit' class='btn btn-danger'>Brisanje</button>".
						    			"</form>".
						    		"</td>";
						    	echo "</tr>";	
						    }
						} else {
						    echo "0 redova";
						}
					?>
				</tbody>
			</table>
		</div>

		<!-- Javascript -->
		<script src="js/jquery-3.2.1.js"></script>
		<script src="bootstrap/js/bootstrap.js"></script>
		<script src="js/main.js"></script>
	</body>

</html>
