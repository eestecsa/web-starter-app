<section id="cover">
    <div class="container">
        <!-- Tekst sa lijeve strane -->
        <div class="col-md-6 col-sm-12 col-xs-12 cover-left-side">
            <h3>Power your future</h3>
            <h1>EESTEC LC Sarajevo</h1>

            <span>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce justo enim, euismod quis varius sit amet, ornare nec dolor. Mauris et sem nec orci ullamcorper tincidunt. Cras tempor, magna sed tristique laoreet, sem tellus luctus lacus, et malesuada velit risus in odio. Nulla a dolor posuere ligula feugiat accumsan. Integer at neque sed purus consectetur bibendum. Nunc sit amet tempor purus, eu suscipit arcu. Mauris feugiat gravida massa, euismod venenatis libero porttitor id. Suspendisse potenti. Nam volutpat, purus et suscipit auctor, metus eros iaculis nulla, et pretium dui elit interdum sem. Aliquam erat volutpat. Nunc finibus rhoncus nibh eget malesuada. Etiam mattis erat ac magna tempor convallis.
            </span>
        </div>

        <!-- Ikonica i slika rakete sa desne strane -->
        <div class="col-md-6 col-sm-12 col-xs-12">
            <img style="width:100%;" src="img/rocket-image.png" />
        </div>
    </div>
</section>