<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12 col-md-offset-4 contact-head">
                <img src="img/contact.png" style="display:block;margin:0 auto;" />

                <h2>Contact us!</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2 contact-des">
                    We really like to talk! If you want to talk about our projects, members and all the opportunities <br />that EESTEC provides for the young people, send as a message!
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12 col-md-offset-4">
                <form action="" method="post">
                    <div class="form-group">
                        <input type="email" class="form-control form-control-contact" placeholder="Your e-mail adress" />
                    </div>

                    <div class="form-group">
                        <textarea class="form-control message-text" placeholder="Your message"></textarea>
                    </div>

                    <input type="submit" class="contact-btn" value="Send" />
                </form>
            </div>
        </div>
    </div>
</section>