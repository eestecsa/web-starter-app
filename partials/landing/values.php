<section class="values">
    <div class="container values-container">
        <!-- Core values heading -->
        <div class="col-md-12 col-sm-12 col-xs-12 values-container-heading">
            <h2>Core values</h2>
                <hr/>
            <span>
                EESTEC is open to every student from the field of Electrical Engineering and Information Technology and similar fields. We would like to encourage you to find a Commitment or found a new commitment (Observer) in your city.
            </span>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-12 values-container-item">
            <img src="img/globe-icon.png" />

            <h3>Culture and Traveling</h3>

            <span>
                On EESTEC events, students from different cultures and backgrounds come together to broaden their knowledge. Workshops and exchanges main purpose is to give them a newly polished skill set that will help them throughout their career and life. 
                    <br /><br />
                For example, during the workshop “Project A²” in Novi Sad, Serbia, 15 students from 15 Nowadays, being able to work in an international environment is compulsory for people who want to succeed, therefore, in EESTEC you will gain important and job-relevant experience.
            </span>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-12 values-container-item">
            <img src="img/skills-icon.png" />

            <h3>Skills and Experiences</h3>

            <span>
                Being an EESTEC member will not only open up new horizons, but it will also help you grow. Balancing the responsibilities of an organization with your university courses teaches you to manage your time and yourself. Being able to manage your time to accomplish all your work will be priceless in your future career.EESTEC means working in teams, therefore it will help you become a team player, enhance your communication skills and openness. 
                    <br /><br />
                You can get involved in international teams such as IT Team and Design Team and develop your digital skills and upgrade your resume. 
            </span>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-12 values-container-item">
            <img src="img/network-icon.png" />

            <h3>Friends and Network</h3>

            <span>
                In the past, for many of our members EESTEC has become a building block for their entire future. It is here that life long friendships and acquaintances have begun. Building a network of highly skilled contacts will be highly advantageous to your future carreer and you.EESTEC gives you also the opportunity to socialize with Alumni, seniors who have gratuated that can advise you and become your mentors.
            </span>
        </div>
    </div>
</section>