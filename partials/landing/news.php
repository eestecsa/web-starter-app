<section id="news-heading">
    <div class="container">
        <div class="col-md-12 col-sm-12 col-xs-12 news-heading-content">
                <h2>What is happening?</h2>
                <h3>EESTEC News & Blog</h3>
        </div>
    </div>
</section>


<section id="news-content">
    <div class="container news-container">
        <?php 
            $queryResult = $db->query("SELECT * FROM novosti LIMIT 6");

            foreach($queryResult as $record){
            
        ?>
            <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="news-item">
                        <h3><?= $record['naslov'] ?></h3>

                        <span class="news-item-des">
                            <?= $record['sadrzaj'] ?>
                        </span>

                        <a href="#" class="news-item-btn">
                            See more 
                        </a>
                    </div>
            </div>
         <?php } ?>
    </div>
</div>