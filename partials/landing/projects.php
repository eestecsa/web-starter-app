<section id="projects">
    <div class="container">
        <!-- Projects heading -->
        <div class="col-md-12 col-sm-12 col-xs-12 projects-heading">
            <h2>Projects</h2>
                <hr/>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-12">
            <a href="http://www.jobfair.ba">
                <img style="width: 100%;" src="img/jobfair-logo.png" />
            </a>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-12">
            <a href="http://www.softskillsacademy.ba">
                <img style="width: 100%;" src="img/ssa-logo.png" />
            </a>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-12">
            <a href="http://www.valueup.ba">
                <img style="width: 100%;" src="img/valueup-logo.png" />
            </a>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12 projects-quote">
            We crate projects with passion, love and dedication.<br />
            We are students, but we work like professsionals!
        </div>
    </div>
</section>
