<section id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 col-md-offset-3 footer-link-list">
                <a href="#">Facebook</a> |
                <a href="#">Instagram</a> |
                <a href="#">Linkedin</a>
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12 col-md-offset-3 footer-link-list">
                <a href="#">JobFAIR</a> |
                <a href="#">Soft Skills Academy</a> |
                <a href="#">ValueUp</a>
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12 col-md-offset-3 footer-link-list">
                All rights reserved @EESTEC LC Sarajevo
            </div>
        </div>
    </div>
</section>