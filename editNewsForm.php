<?php 
	include "brains/dbConnection.php";

	session_start();
	if(!isset($_SESSION['korisnickoIme'])){
		header("location: login.php");
	}

	$naslov = "";
	$sadrzaj = "";
	$slika = "";
	$idNovostiZaEdit = "";

	if($_POST) {	
		$idNovostiZaEdit = $_POST["id"];
		$sql = $con->prepare("SELECT naslov, sadrzaj, slika FROM novosti WHERE id = ?");
		$sql->bind_param("s", $idNovostiZaEdit);
		$sql->execute();
		$sql->bind_result($naslov, $sadrzaj, $slika);
		$sql->fetch();
	}
?>


<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
		<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>Izmijeni novost</title>

		<!-- CSS -->
		<link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="css/main.css">

	</head>
	<body>
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <a class="navbar-brand" href="#">Admin panel</a>
		    </div>
		    <ul class="nav navbar-nav">
		      <li class="active"><a href="allNews.php">Novosti</a></li>
		      <li><a href="addNewsForm.php">Dodaj novost</a></li>
		    </ul>
		  </div>
		</nav>

		<div class="container">
			<form action="admin/editNews.php" method="POST">
				<input type="text" name="id" style="display:none;" value="<?= $idNovostiZaEdit ?>">
		      <div class="form-group">
		        <label for="naslov">Naslov:</label>
		        <input type="textl" class="form-control" id="naslov" placeholder="Unesite naslov" name="naslov" value="<?php echo $naslov ?>">
		      </div>
		      <div class="form-group">
		        <label for="sadrzaj">Sadrzaj:</label>
		        <input type="text" class="form-control" id="sadrzaj" placeholder="Unesite sadrzaj" name="sadrzaj" value="<?= $sadrzaj ?>">
		      </div>
		      <div class="form-group">
		        <label for="slika">Slika:</label>
		        <input type="text" class="form-control" id="slika" placeholder="Uniste putanju do slike" name="slika" value="<?= $slika ?>">
		      </div>
		      <button type="submit" class="btn btn-default">Potvrdite</button>
		    </form>
		</div>

		<!-- Javascript -->
		<script src="js/jquery-3.2.1.js"></script>
		<script src="bootstrap/js/bootstrap.js"></script>
		<script src="js/main.js"></script>
	</body>
</html>
