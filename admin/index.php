<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
		<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>Game of codes</title>

		<!-- CSS -->
		<link rel="stylesheet" href="../bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="../css/main.css">
		<link rel="stylesheet" href="css/admin.css">

	</head>
	<body>

		<form role="form" method="POST" enctype="multipart/form-data" action="../brains/upload.php">
			<label for="naslov">Naslov</label>
			<input type="text" name="naslov" />

			<label for="sadrzaj">Sadrzaj</label>
			<textarea name="sadrzaj"></textarea>

			<label for="slika">Slika</label>
			<input type="file" name="slika" />
		</form>

		<!-- Javascript -->
		<script src="../js/jquery-3.2.1.js"></script>
		<script src="../bootstrap/js/bootstrap.js"></script>
		<script src="../js/main.js"></script>
		<script src="js/admin.js"></script>
	</body>
</html>
