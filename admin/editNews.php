<?php
	include "brains/dbConnection.php";

	$idNovostiZaEdit = "";
	$noviNaslov = "";
	$noviSadrzaj = "";
	$novaSlika = "";
	if($_POST){
		$idNovostiZaEdit = $_POST["id"];
		$noviNaslov = $_POST["naslov"];
		$noviSadrzaj = $_POST["sadrzaj"];
		$novaSlika = $_POST["slika"]; 

		$sql = $con->prepare("UPDATE novosti SET naslov = ?, sadrzaj = ?, slika = ? WHERE id = ?");
		$sql->bind_param("ssss", $noviNaslov, $noviSadrzaj, $novaSlika, $idNovostiZaEdit);
		$sql->execute();
		

		if($sql->errno){
			echo "FAILURE: ".$sql->errno. " ". $sql->error;
		} else echo "IZMJENA USPJESNO IZVRSENA";
	}

	mysqli_close($con);
?>