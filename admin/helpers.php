<?php
	function printArray($array, $pad=''){
    	foreach ($array as $key => $value){
	        echo $pad . "$key => $value";
	        if(is_array($value)){
	            printArray($value, $pad.' ');
	        }  
    	} 
	}
?>