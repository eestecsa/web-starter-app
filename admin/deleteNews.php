<?php 
	include "brains/dbConnection.php";
	
	$idNovostiZaBrisanje = "";
	if($_POST){
		$idNovostiZaBrisanje = $_POST["id"];
		$sql = $con->prepare("DELETE FROM novosti WHERE id = ?");
		$sql->bind_param("s", $idNovostiZaBrisanje);
		$sql->execute();

		if($sql->errno){
			echo $sql->error;
		} else {
			echo "BRISANJE USPJESNO IZVRSENO";
		}
	}

	mysqli_close($con);
?>