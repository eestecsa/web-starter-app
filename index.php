<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
		<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>Game of codes</title>

		<!-- CSS -->
		<link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="css/main.css">

	</head>
	<body>
		<?php
			include "brains/database.php";
			$db = Database::connect();
			echo Database::ispisiNesto();

			

			
		?>
		<?php include 'partials/menu.php' ?>

		<!-- Ohri ti popuni ovo izmedju, ne znam kako si zamislio -->
		<?php
			include 'partials/landing/cover.php';
			include 'partials/landing/values.php';
			include 'partials/landing/projects.php';
			include 'partials/landing/news.php';
			include 'partials/landing/contact.php';
		?>

		<?php include 'partials/footer.php' ?>
		<?php Database::disconnect(); ?>

		<!-- Javascript -->
		<script src="js/jquery-3.2.1.js"></script>
		<script src="bootstrap/js/bootstrap.js"></script>
		<script src="js/main.js"></script>
	</body>
</html>
