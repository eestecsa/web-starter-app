<?php
	session_start();
	if(!isset($_SESSION['korisnickoIme'])){
		header("location: login.php");
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
		<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>Dodaj novost</title>

		<!-- CSS -->
		<link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="css/main.css">

	</head>
	<body>
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <a class="navbar-brand" href="#">Admin panel</a>
		    </div>
		    <ul class="nav navbar-nav">
		      <li><a href="allNews.php">Novosti</a></li>
		      <li class="active"><a href="addNewsForm.php">Dodaj novost</a></li>
		    </ul>
		  </div>
		</nav>

		<div class="container">
			<form action="admin/addNews.php" method="POST">
		      <div class="form-group">
		        <label for="naslov">Naslov:</label>
		        <input type="text" class="form-control" id="naslov" placeholder="Unesite naslov" name="naslov">
		      </div>
		      <div class="form-group">
		        <label for="sadrzaj">Sadrzaj:</label>
		        <input type="text" class="form-control" id="sadrzaj" placeholder="Unesite sadrzaj" name="sadrzaj">
		      </div>
		      <div class="form-group">
		        <label for="slika">Slika:</label>
		        <input type="text" class="form-control" id="slika" placeholder="Uniste putanju do slike" name="slika">
		      </div>
		      <button type="submit" class="btn btn-default">Potvrdite</button>
		    </form>
		</div>

		<!-- Javascript -->
		<script src="js/jquery-3.2.1.js"></script>
		<script src="bootstrap/js/bootstrap.js"></script>
		<script src="js/main.js"></script>
	</body>
</html>
